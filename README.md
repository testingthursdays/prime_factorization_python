Prime Factors
=============

Write a program that, given a positive integer, returns a list of the integers prime factors.